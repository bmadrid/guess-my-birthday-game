
#imported function to guess a random integer within a range
from random import randint

#defined parameters for randint guessing year and month
low_year = 1924
high_year = 2004
low_month = 1
high_month = 12

#defines variable for response of later or earlier year or month
later_or_earlier_month = ""
later_or_earlier_year = ""

#user puts in their name
user_name = input("What is your name?\n")

#empty list to store guesses inside
month_guess_list = []
year_guess_list = []

#for loop that runs through the game guessing 5 times and improving based on user input
for guess_number in range(5):

    #creating variables to store previous guess
    month_guess = randint(low_month, high_month)
    year_guess = randint(low_year, high_year)

    #prints the random guess of birthday
    print(guess_number+1,": ",user_name, "is your birthday on ", month_guess, "/", year_guess,"?")

    #asks user if computer guess was correct
    answer = input("Yes or no?\n")

    #conditional if guess was correct
    if answer.lower() == "yes":
        print("I knew it!")

        #break for loop if already guessed appropriately
        break

    #conditional if guess is wrong and we have not reached the final guess
    elif answer.lower() == "no" and guess_number <4:
        print("Drat! Lemme try again!")

        #asks user if the month was correct or should be later or earlier if it was not correct last guess
        if later_or_earlier_month.lower() != "correct":
            later_or_earlier_month = input("Was the month correct or were you born in a later or earlier month?\n")

        #conditional shortening the range of month based on if it was too high or too low of a guess
        if later_or_earlier_month.lower() == "later":

            #adds guess and too early if the month was too early
            month_guess_list.append([month_guess, "too early"])

            #adjusts lower range of month guess to be 1 higher if month was too early
            month_guess += 1
            low_month = month_guess

        #conditional shortening the range of month based on if it was too high or too low of a guess
        elif later_or_earlier_month.lower() == "earlier":

            #adds guess to list and too late if month was too far ahead
            month_guess_list.append([month_guess, "too late"])

            #adjusts high range for the random month guess
            month_guess -= 1
            high_month = month_guess

        #conditional for if the month was correctly guessed
        elif later_or_earlier_month.lower() == "correct":

           #adds guess of month and correct month if month is correctly guessed
           month_guess_list.append([month_guess, "correct month"])

           #adjusts range for month guess to only be chosen as correct month
           high_month = month_guess
           low_month = month_guess

        #conditional in case user input doesn't match yes, no, or correct
        else:
            print("You're trying to keep me from guessing it correct aren't you?")

            #adds guess to list and user input that didn't match yes, no, or correct
            month_guess_list.append([month_guess, "user said: {}".format(later_or_earlier_month)])

        #asks user if the year was correct or should be later or earlier if it was not previously correct
        if later_or_earlier_year.lower() != "correct":
            later_or_earlier_year = input("Was the year correct or were you born in a later or earlier year?\n")

        #conditional shortening the range of year based on if it was too high or too low of a guess
        if later_or_earlier_year.lower() == "later":

            #adds guess for year and too early if the year  was too early
            year_guess_list.append([year_guess, "too early"])

            #increases the lower range of year guess to be 1 higher if year guessed was too early
            year_guess += 1
            low_year = year_guess

        #conditional shortening range of year based on if it was too high or low of a guess
        elif later_or_earlier_year.lower() == "earlier":

            #adds guess for year and too late if the year guessed was too late
            year_guess_list.append([year_guess, "too late"])

            #decreases the higher range of year guess to be 1 lower if year guessed was too late
            year_guess -= 1
            high_year = year_guess

        #conditional for if year was correctly guessed
        elif later_or_earlier_year.lower() == "correct":

           #adds guess for year and correct year if year guessed was correct
           year_guess_list.append([year_guess, "correct year"])

           #adjusts range for year guess to only guess the correct year
           high_year = year_guess
           low_year = year_guess

        #conditional in case user input doesn't match yes, no, or correct
        else:

            #adds guess to list and user input that didn't match yes, no, or correct
            year_guess_list.append([year_guess, "user said: {}".format(later_or_earlier_year)])
            print("You're trying to keep me from guessing it correct aren't you?")

    #conditional if we have completed our final guess
    if guess_number == 4:
        print("I have other things to do. Good bye.")

#tells user how many times computer guessed
print("I guessed ", len(month_guess_list) + 1, "times!")

#loop telling user month guess history and whether it was correct, too early, or too late
for guess, issue in month_guess_list:
    print("I guessed:", guess, "and the month was", issue)

#loop telling user year guess history and whether it was correct, too early, or too late
for guess, issue in year_guess_list:
    print("I guessed:", guess, "and the year was", issue)
