
## Name
Guess My Birthday Game

## Description
The Guess My Birthday Game that I have created will randomly guess a birth year and month for the user and will adjust it's next guess based on user input regarding previous guess

## Authors and acknowledgment
Bradley Madrid - Student at Hack Reactor

## Project status
Project is currently being improved and further developed
